# cypress-api-testing

## **_Configuração do ambiente_**

- Clonar o repositório `git clone https://gitlab.com/mceliv/cypress-api-testing.git`

## Use

- Instale as dependências com 
```
    npm install
```
<br>

## **_Tecnologias utilizadas_**

- Cypress ^5.6.0
- Allure ^1.8.5

## **_Cypress_** 

O arquivo de configuração do cypress `cypress.json`pode ser encontrado na raiz do projeto e com a seguinte configuração. 
Na configuração abaixo e possível verificar os seguintes dados:

* Que tipo de testes irão ser rodados (todos que tenham um .spec)
* `watchForFileChanges` se estiver habilitado, ele irá rodar o teste a cada alteração salva no código (se extiver usando o cypress open)
* Configuração do reporte, informando qual arquivo de configuração deve ser usado,
* O browser que será rodado os testes

```
    {
        "baseUrl": "http://challengeqa.staging.devmuch.io/",
        "defaultCommandTimeout": 15000,
        "retryOnStatusCodeFailure": true,
        "retries": 0,
        "watchForFileChanges": false,
        "testFiles": "**/*.*spec.*",
        "video": false,
        "browser": "Chrome",
        "chromeWebSecurity": false,
        "viewportWidth": 1280,
        "viewportHeight": 720
    }
```


### **_Arquitetura de teste_**

- fixtures: arquivos que serão usados nos teste
- integration: cenários de teste ficam dentro desta pasta
- plugins: se for necessário utilizar plugin, eles deve estar instaciados aqui 
- support: aqui dentro desta pasta possui o core da automação
    - commands: todas as funcionalidades que são utilizadas em mais de um lugar nos testes estão escritas aqui.

```
    ├── Cypress
        ├── fixtures 
        ├── integration
        ├── plugins
            ├── index.js
        ├── support
            ├── commands.js 
        ├── README.md  
```
<br>

## **_Rodar os testes_**

Para executar os testes basta usar um dos comando abaixo

* Para abrir o cypress: Ele irá abrir o cypress com a configuração do ambiente de teste 

```
npm run cypress:open
```

* Para rodar todos os testes basta seguir o exemplo.

```
npm run cy:run
```

* Para limpar relatórios antigos do Allure.

```
npm run allure:clear
```

* Para gerar relatório usando Allure.

```
npm run allure:generate
```

* Para executar os comandos nessa ordem:
```
    ├── Limpar os relatório antigos do Allure
    ├── Executar os teste 
    ├── Gerar o resultado usando Allure
```
basta executar o seguinte comando.

```
npm run test
```

* Para abrir relatório usando Allure.

```
npm run allure:open
```

### Alguns artigos interessantes:

- Documentação sobre a ferramenta CYPRESS é encontrada na página a seguir [LINK](https://www.cypress.io/)
- Documentação sobre a ferramenta ALLURE é encontrada na página a seguir [LINK](https://docs.qameta.io/allure/)

### Autor:
Feito por Mônica C. Eli 👋🏽 [Entre em contato!](https://www.linkedin.com/in/monica-eli/)

[![pipeline status](https://gitlab.com/mceliv/cypress-api-testing/badges/master/pipeline.svg)](https://gitlab.com/mceliv/cypress-api-testing/-/commits/master)
