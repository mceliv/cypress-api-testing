Cypress.Commands.add("get_status", (url, expectedStatus) => { 
    cy.request({method: 'GET',  url: url,
    failOnStatusCode: false
      }).then((response) => {
          expect(response.status).to.eq(expectedStatus)
      });
})

Cypress.Commands.add("get_resultBody", (url) => { 
    cy.request(url)
    .its('body');
})