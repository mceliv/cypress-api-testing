import { Context } from '../fixtures/context'
const loadFixture = new Context();

const urlPositiveValidPtBr = loadFixture.value.urlPositiveValidPtBr;
const numberInFullPositivePtBr = loadFixture.value.numberInFullPositivePtBr;
const urlNegativeValidPtBr = loadFixture.value.urlNegativeValidPtBr;
const numberInFullNegativePtBr = loadFixture.value.numberInFullNegativePtBr;
const urlPositiveValidEn = loadFixture.value.urlPositiveValidEn;
const numberInFullPositiveEn = loadFixture.value.numberInFullPositiveEn;
const urlNegativeValidEn = loadFixture.value.urlNegativeValidEn;
const numberInFullNegativeEn = loadFixture.value.numberInFullNegativeEn;
const urlInvalidPositive = loadFixture.value.urlInvalidPositive;
const urlInvalidNegative = loadFixture.value.urlInvalidNegative;

describe('test values', () => {
  it('Return is a JSON', () => {
    cy.request(urlPositiveValidPtBr)
    .its('headers')
    .its('content-type')
    .should('include', 'application/json');
  });
  
  it('Positive value valid in Portuguese', () => {
    cy.get_status(urlPositiveValidPtBr, 200);
    cy.get_resultBody(urlPositiveValidPtBr)
      .should('deep.eq', numberInFullPositivePtBr)
  });

  it('Negative value valid in Portuguese', () => {
    cy.get_status(urlNegativeValidPtBr, 200);
    cy.get_resultBody(urlNegativeValidPtBr)
      .should('deep.eq', numberInFullNegativePtBr)
  });

  it('Positive value valid in English', () => {
    cy.get_status(urlPositiveValidEn, 200);
    cy.get_resultBody(urlPositiveValidEn)
      .should('deep.eq', numberInFullPositiveEn)
  });

  it('Negative value valid in English', () => {
    cy.get_status(urlNegativeValidEn, 200);
    cy.get_resultBody(urlNegativeValidEn)
      .should('deep.eq', numberInFullNegativeEn)
  });

  it('Value invalid', () => {
    cy.get_status(urlInvalidPositive, 400);
    cy.get_status(urlInvalidNegative, 400);
  });
});
